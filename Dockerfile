FROM prom/prometheus:latest

COPY ./prometheus/prometheus.yml /etc/prometheus/prometheus.yml
COPY ./prometheus/alert.rules /etc/prometheus/alert.rules
COPY ./prometheus/prometheus_data /prometheus

CMD [ "--config.file=/etc/prometheus/prometheus.yml", "--storage.tsdb.path=/prometheus", "--web.console.libraries=/etc/prometheus/console_libraries", "--web.console.templates=/etc/prometheus/consoles", "--web.enable-lifecycle" ]